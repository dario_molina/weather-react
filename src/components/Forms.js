import React,{Component} from 'react';

class Form extends Component {

  constructor(props) {
    super(props);
    this.countryRef = React.createRef();
    this.cityRef = React.createRef();
  }

  searchWeather = (e) => {
    e.preventDefault();
    let response = {
      city: this.cityRef.current.value,
      country: this.countryRef.current.value,
    }
    this.props.queryWeather(response);
  }

  render() {
    return (
      <div className="contenedor-form">
        <div className="container">
          <div className="row">
            <form className="col s12" onSubmit={this.searchWeather}>
              <div className="row">
                <div className="input-field col s5">
                  <select ref={this.countryRef}>
                    <option value="">--</option>
                    <option value="AR">Argentina</option>
                    <option value="CO">Colombia</option>
                    <option value="CR">Costa Rica</option>
                    <option value="ES">España</option>
                    <option value="US">Estados Unidos</option>
                    <option value="MX">México</option>
                    <option value="PE">Perú</option>
                  </select>
                  <label>Selecciona un país</label>
                </div>
                <div className="input-field col s5">
                  <input ref={this.cityRef} id="city_id" type="text" data-length="10"/>
                  <label htmlFor="city_id">Ciudad</label>
                </div>
                <div className="input-field col s2">
                  <button className="btn waves-effect waves-light" type="submit" name="action">Buscar
                    <i className="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>  
      </div>
    );
  }
}

export default Form