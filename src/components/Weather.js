import React,{Component} from 'react';

class Weather extends Component {

  kelvinToCelsius = (temp) => {
    const kelvin = 273.15;    
    return (temp - kelvin).toFixed(2);
  }

  getWeather = () => {
    let {name, main, weather, wind} = this.props.weather;
    let res = {
      name: name,
      temp_current: this.kelvinToCelsius(main.temp),
      temp_min: this.kelvinToCelsius(main.temp_min),
      temp_max: this.kelvinToCelsius(main.temp_max),
      pressure: main.pressure,
      humidity: main.humidity,
      wind: wind.speed,
      icon: `https://openweathermap.org/img/w/${weather[0].icon}.png`,
    }
    return res
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col s6 offset-s3">
            <div className="card blue">
              <div className="card-content">
                <span className="white-text">
                  <h2 className="center-align">{this.getWeather().name}</h2>
                    <h5 className="valign-wrapper">
                      Actual: {this.getWeather().temp_current} °C
                      <img src={this.getWeather().icon} alt={this.getWeather().name} />
                    </h5>
                    <h5 className="valign-wrapper">
                      Mín: {this.getWeather().temp_min} °C
                    </h5>
                    <h5 className="valign-wrapper">
                      Máx: {this.getWeather().temp_max} °C
                    </h5>
                    <table className="highlight">
                      <thead>
                        <tr>
                          <th>Presión</th>
                          <th>Humedad</th>
                          <th>Viento</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{this.getWeather().pressure} Pa</td>
                          <td>{this.getWeather().humidity} %</td>
                          <td>{this.getWeather().wind} km/h</td>
                        </tr>
                      </tbody>
                    </table>    


                </span>  
              </div>
            </div>
          </div>  
        </div>  
      </div>
    );
  }
}

export default Weather;