import React,{Component} from 'react';

class Error extends Component {

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col s6 offset-s3">
            <div className="card-panel red center-align">
              <span className="white-text">
                {this.props.msg}
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Error
