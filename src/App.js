import React, { Component } from 'react';
import Header from './components/Header';
import Form from './components/Forms';
import Weather from './components/Weather';
import Error from './components/Error';


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: false,
      data: {},
      weather: {},
    }
  }

  queryWeather = (response) => {
    if (response.country==='' || response.city==='') {
      this.setState({error: true});
    } 
    else {
      this.setState({
        error: false,
        data: response,
      });
      this.apiQueryWeather(response.country, response.city);
    }
  }

  apiQueryWeather = (country, city) => {
    if (!country || !city) return null;
    const appid = '8063b29a44129867685a966c5b59a4ad';
    let url = `https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${appid}`;
    fetch(url)
      .then(response => {
        return response.json();
      })
      .then(data => {
        this.setState({weather: data});
      })
      .catch(error => {
        console.log(error);
      })
  }

  render() {
    let result = null;
    if (!this.state.error && this.state.weather.id) {
      result = <Weather weather={this.state.weather}/>
    } else if (this.state.error) {
      result = <Error msg='Existen campos vacíos.'/>
    }

    return (
      <div className="app">  
        <Header
          title='Clima'
        />
        <Form
          queryWeather={this.queryWeather}
        />
        {result}
      </div>
    );
  }
}

export default App;
